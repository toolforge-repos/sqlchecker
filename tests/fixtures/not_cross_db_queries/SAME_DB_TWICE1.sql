SELECT *
FROM svwiki_p.recentchanges
JOIN svwiki_p.user
ON rc_user = user_id
WHERE rc_namespace IN (1,3,5,7,9,11)
AND user_registration > 20140101000001
AND user_name NOT IN (
    SELECT REPLACE(log_title,"_"," ")
    FROM svwiki_p.logging
    WHERE log_type = "block" AND log_action = "block"
    AND log_timestamp > 20140101000001
) ORDER BY rc_timestamp DESC
LIMIT 10;
