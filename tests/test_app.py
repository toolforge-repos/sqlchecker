import os

import pytest
from flask import url_for

from sqlchecker import create_app

FIXTURES_DIR = "tests/fixtures"


@pytest.fixture
def app():
    return create_app()


def get_query_files(queries_dir: str):
    query_files = [
        f.name
        for f in os.scandir(queries_dir)
        if f.is_file() and f.name.endswith(".sql")
    ]
    for query_file in query_files:
        yield os.path.join(queries_dir, query_file)


@pytest.fixture(
    params=get_query_files(queries_dir=f"{FIXTURES_DIR}/not_cross_db_queries")
)
def not_cross_db_query(request):
    with open(request.param, "rb") as query_fd:
        query = query_fd.read()

    return {"sql": query.decode("utf8")}


@pytest.fixture(params=get_query_files(queries_dir=f"{FIXTURES_DIR}/cross_db_queries"))
def cross_db_query(request):
    with open(request.param, "rb") as query_fd:
        query = query_fd.read()

    return {"sql": query.decode("utf8")}


@pytest.fixture(params=get_query_files(queries_dir=f"{FIXTURES_DIR}/good_queries"))
def good_query(request):
    with open(request.param, "rb") as query_fd:
        query = query_fd.read()

    return {"sql": query.decode("utf8")}


@pytest.fixture(params=get_query_files(queries_dir=f"{FIXTURES_DIR}/bad_queries"))
def bad_query(request):
    with open(request.param, "rb") as query_fd:
        query = query_fd.read()

    return {"sql": query.decode("utf8")}


def test_valid_sql(good_query, client):
    response = client.post(url_for("api.validate"), json=good_query)
    assert response.status_code == 200
    assert "result" in response.json
    assert "errors" in response.json["result"]
    assert "is_valid" in response.json["result"]
    assert response.json["result"]["errors"] == []
    assert response.json["result"]["is_valid"] is True


def test_non_valid_sql(bad_query, client):
    response = client.post(url_for("api.validate"), json=bad_query)
    assert response.status_code == 200
    assert "result" in response.json
    assert "is_valid" in response.json["result"]
    assert "errors" in response.json["result"]
    assert response.json["result"]["is_valid"] is False
    assert response.json["result"]["errors"]


def test_cross_database_queries(cross_db_query, client):
    print(cross_db_query)
    response = client.post(url_for("api.is_cross_database"), json=cross_db_query)
    assert response.status_code == 200
    assert "result" in response.json
    assert response.json["result"] is True


def test_no_cross_database_queries(not_cross_db_query, client):
    print(not_cross_db_query)
    response = client.post(url_for("api.is_cross_database"), json=not_cross_db_query)
    assert response.status_code == 200
    assert "result" in response.json
    assert response.json["result"] is False
