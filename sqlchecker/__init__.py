#!/usr/bin/env python3
from flask import Flask, Response

from sqlchecker.api import api


def create_app():
    app = Flask(__name__)
    app.register_blueprint(api, url_prefix="/api/v1")

    @app.route("/")
    def root():
        return Response(
            "SQLChecker allows to run some checks on sql queries.",
            content_type="text/plain",
        )

    return app
