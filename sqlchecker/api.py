#!/usr/bin/env python3
from typing import Dict, Optional

from flask import Blueprint, jsonify, request
from werkzeug.exceptions import BadRequest

from sqlchecker import checks

api = Blueprint("api", __name__)


def _extract_sql(maybe_json: Optional[Dict[str, str]]) -> str:
    if not maybe_json:
        raise BadRequest(
            (
                "Invalid data passed, expecting a json blob with the key "
                f"'sql' as string, got data: {request.data}\n\n"
                "Maybe you forgot to add the 'Content-Type: application/json' "
                "header?"
            ),
        )

    data = maybe_json.get("sql", None)
    if data is None:
        raise BadRequest(
            (
                "Invalid data passed, expecting a json blob with the key "
                f"'sql' as string, got json: {request.json}"
            ),
        )

    return data


@api.route("/validate", methods=["POST"])
def validate():
    data = _extract_sql(request.json)
    result = checks.validate(data)
    return jsonify({"result": result})


@api.route("/is_cross_database", methods=["POST"])
def is_cross_database():
    data = _extract_sql(request.json)
    result = checks.is_cross_database(data)
    return jsonify({"result": result})
