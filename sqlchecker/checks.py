#!/usr/bin/env python3
import logging
from typing import Set

import moz_sql_parser
import sqlparse
import sqlvalidator


def validate(sql: str) -> bool:
    parsed_query = sqlvalidator.parse(sql)
    return {
        "is_valid": parsed_query.is_valid(),
        "errors": parsed_query.errors,
    }


def _has_join_moz_sql_parse(parsed_query) -> bool:
    for from_stmt in parsed_query.get("from", []):
        if isinstance(from_stmt, dict):
            if "join" in from_stmt:
                return True
    return False


def _has_join_sqlparse(parsed_query) -> bool:
    return any("join" == token.value.lower() for token in parsed_query.tokens)


def _get_from_stmts(parsed_query):
    if (
        isinstance(parsed_query, str)
        or isinstance(parsed_query, int)
        or isinstance(parsed_query, bool)
        or parsed_query is None
    ):
        return []

    if isinstance(parsed_query, dict):
        from_stmts = []
        for key, elem in parsed_query.items():
            if key.lower() == "from":
                from_stmts.append(elem)

            else:
                from_stmts.extend(_get_from_stmts(elem))
        return from_stmts

    if isinstance(parsed_query, list):
        from_stmts = []
        for elem in parsed_query:
            from_stmts.extend(_get_from_stmts(elem))

        return from_stmts

    raise Exception(f"Unsupported sql format... {parsed_query}")


def _get_join_elements(from_stmt):
    if isinstance(from_stmt, str):
        return [from_stmt]

    if isinstance(from_stmt, dict):
        if "join" not in from_stmt:
            raise Exception(f"Unsupported sql format... {from_stmt}")

        return _get_join_elements(from_stmt["join"])

    if isinstance(from_stmt, list):
        joined_elements = []
        for elem in from_stmt:
            joined_elements.extend(_get_join_elements(elem))

        return joined_elements

    raise Exception(f"Unsupported sql format... {from_stmt}")


def _get_dbs_from_parsed_query_moz_sql_parse(parsed_query) -> bool:
    LOGGER = logging.getLogger(__name__)
    all_identifiers = []
    for from_stmt in _get_from_stmts(parsed_query):
        all_identifiers.extend(_get_join_elements(from_stmt))

    LOGGER.info("#####  Identifiers(moz-sql): %s", all_identifiers)

    dbs = set()
    for identifier in all_identifiers:
        if "." in identifier:
            dbs.add(identifier.split()[0].split(".", 1)[0])
        else:
            # Directly accessing the table, adding an implicit DB entry
            dbs.add("implicit_db")

    LOGGER.info("###!#!#! Found DBs(moz-sql): %s", dbs)
    return dbs


def _has_multiple_db_identifiers_moz_sql_parse(parsed_query) -> bool:
    return len(_get_dbs_from_parsed_query_moz_sql_parse(parsed_query)) > 1


def _get_dbs_from_parsed_query_sqlparse(parsed_query) -> Set[str]:
    LOGGER = logging.getLogger(__name__)
    probably_db_identifiers = set()
    for token in parsed_query.tokens:
        if isinstance(token, sqlparse.sql.Identifier) and "." in token.value:
            probably_db_identifiers.add(token.value.split()[0].split(".")[0])
            continue
        elif isinstance(token, sqlparse.sql.Identifier):
            # Directly accessing the table, adding an implicit DB entry
            LOGGER.info("Got implitic db: %s", token)
            probably_db_identifiers.add("implicit_db")
            continue

        if token.value.upper() == "WHERE":
            break

    LOGGER.debug(
        "##### DB identifiers (sqlparse): %s",
        probably_db_identifiers,
    )
    return probably_db_identifiers


def get_dbs_from_sql(sql: str) -> Set[str]:
    dbs = None
    try:
        parsed_query = moz_sql_parser.parse(sql)
        dbs = _get_dbs_from_parsed_query_moz_sql_parse(parsed_query)
    except Exception:
        pass

    if dbs is None:
        parsed_queries = sqlparse.parse(sql)
        dbs = set()
        for parsed_query in parsed_queries:
            dbs.union(_get_dbs_from_parsed_query_sqlparse(parsed_query))

    return dbs


def _has_multiple_db_identifiers_sqlparse(parsed_query) -> bool:
    return len(_get_dbs_from_parsed_query_sqlparse(parsed_query)) > 1


def _is_cross_database_moz_sql_parse(sql: str) -> bool:
    parsed_query = moz_sql_parser.parse(sql)
    return _has_multiple_db_identifiers_moz_sql_parse(parsed_query)


def _is_cross_database_sqlparse(sql: str) -> bool:
    parsed_queries = sqlparse.parse(sql)

    for parsed_query in parsed_queries:
        if _has_multiple_db_identifiers_sqlparse(parsed_query):
            return True

    return False


def is_cross_database(sql: str) -> bool:
    LOGGER = logging.getLogger(__name__)
    LOGGER.info("Checking for cross db:\n%s", sql)
    result = None
    try:
        result = _is_cross_database_moz_sql_parse(sql)
        if result:
            LOGGER.info("moz-sql-parse found it to be cross-db")
        else:
            LOGGER.info("moz-sql-parse found it not to be cross-db")
    except Exception as error:
        LOGGER.info("moz-sql-parse failed, Falling back to sqlparse, error: %s", error)

    if result is None:
        # sqlparse gives more false positives, so we only check when we got no
        # answer from moz-sql-parse
        result = _is_cross_database_sqlparse(sql)

        if result:
            LOGGER.info("sqlparse found it to be cross-db")
        else:
            LOGGER.info("sqlparse found it not to be cross-db")

    return result
