#!/usr/bin/env python3

from setuptools import setup

URL = "https://toolsadmin.wikimedia.org/tools/id/sqlchecker/projects/id/Tool-sqlchecker"
BUGTRACKER_URL = "https://phabricator.wikimedia.org/project/profile/5085/"


if __name__ == "__main__":
    setup(
        author="David Caro",
        author_email="david@dcaro.es",
        description="Small SQL checker service.",
        setup_requires=["autosemver"],
        install_requires=["autosemver"],
        license="GPLv3",
        name="sqlchecker",
        package_data={"": ["CHANGELOG", "AUTHORS"]},
        packages=["sqlchecker"],
        url=URL,
        autosemver={
            "bugtracker_url": BUGTRACKER_URL,
        },
    )
